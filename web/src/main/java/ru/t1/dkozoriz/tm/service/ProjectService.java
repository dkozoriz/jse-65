package ru.t1.dkozoriz.tm.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkozoriz.tm.model.Project;
import ru.t1.dkozoriz.tm.repository.ProjectRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProjectService {

    private final ProjectRepository repository;

    public List<Project> findAll() {
        return repository.findAll();
    }

    @Transactional
    public Project save(Project task) {
        return repository.save(task);
    }

    @Transactional
    public Project update(Project project) {
        if (!repository.existsById(project.getId())) return null;
        return repository.save(project);
    }

    public Project findById(String task) {
        return repository.findById(task).orElse(null);
    }

    public long count() {
        return repository.count();
    }

    @Transactional
    public void deleteById(String id) {
        repository.deleteById(id);
    }

    @Transactional
    public void deleteAll() {
        repository.deleteAll();
    }

}