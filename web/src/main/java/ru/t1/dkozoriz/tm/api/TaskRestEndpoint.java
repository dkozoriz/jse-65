package ru.t1.dkozoriz.tm.api;

import org.springframework.web.bind.annotation.*;
import ru.t1.dkozoriz.tm.model.Task;

import java.util.List;

public interface TaskRestEndpoint {

    @GetMapping("/getAll")
    List<Task> getAll();

    @GetMapping("/count")
    Long count();

    @GetMapping("/get/{id}")
    Task get(
            @PathVariable("id") String id
    );

    @PostMapping("/post")
    Task post(@RequestBody Task task);

    @PutMapping("/put")
    Task put(
            @RequestBody Task task
    );

    @DeleteMapping("/delete/{id}")
    void delete(
            @PathVariable("id") String id
    );

    @DeleteMapping("/deleteAll")
    void deleteAll();

}